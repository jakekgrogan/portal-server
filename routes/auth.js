const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

// Get secret key from environment variables
const secret = process.env.JWT_SECRET_KEY;

// Setup mailgun api
const mailgun = require('mailgun-js')({
    apiKey: process.env.MAILGUN_KEY,
    domain: process.env.MAILGUN_DOMAIN
});

// Sequelize ORM imports
const Sequelize = require('sequelize');
const sequelize = require('../sequelize');
const Op = Sequelize.Op;

// Model imports
const Student = require('../models/student');
const University = require('../models/university');
const UniversityCourse = require('../models/university_course');
const Course = require('../models/course');
const CourseModule = require('../models/course_module');
const StudentModule = require('../models/student_module');

// Signup route
router.post("/signup", (req, res, next) => {
    bcrypt.hash(req.body.password, 10).then(hash => {
        let emailConfirmationToken = crypto.randomBytes(16);
        emailConfirmationToken = emailConfirmationToken.toString('hex');

        // Define new student
        const student = new Student({
            fname: req.body.fname,
            lname: req.body.lname,
            college_id: req.body.college,
            email: req.body.email,
            password: hash,
            course_code1: "lkjfal",
            course_code2: "lkasjfa",
            emailConfirmationToken: emailConfirmationToken
        });

        //Save student to database
        student.save()
            .then(result => {
                // Send email confirmation
                sendEmailConfirmation(req, emailConfirmationToken);
                res.status(201).json({
                    message: 'A confirmation email has been sent to ' + req.body.email,
                    result: result
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "That email is already registered"
                });
            });
    });
    
    
});


// Login Route
router.post("/login", (req, res, next) => {

    // used for avoid scoping issues
    let fetchedStudent;

    //Find student in database
    Student.findOne({
        where: {email: req.body.email}
    }).then(student => {
        if (!student) {
            return res.status(401).json({
                message: "An account with that email does not exist"
            });
        }
        fetchedStudent = student;
        return bcrypt.compare(req.body.password, student.password);
    }).then(result => {
        if (!result) {
            return res.status(401).json({
                message: "Email or password was incorrect"
            });
        }
        if (fetchedStudent.isEmailConfirmed == null) {
            return res.status(401).json({
                message: "Email not confirmed",
            })
        }

        // Sign JWT Token
        const token = jwt.sign({email: fetchedStudent.email, password: fetchedStudent.password}, secret, { expiresIn: "1h" });
        res.status(200).json({
            token:token,
            expiresIn: 3600,
            uId: fetchedStudent.id,
            modules: fetchedStudent.setupComplete,
        });
    }).catch(err => {
        res.status(401).json({
            message: "Authentication failure, please try again"
        })
    });
});

// Get all courses for a given university
router.post('/getCourses', (req, res, next) => {
    UniversityCourse.findAll({
        where: { university_id: req.body.courseId }
    }).then(courses => {
            let courseList = courses;
            let idList = [];
            for (i = 0; i < courseList.length; i++) {
                idList.push(courseList[i].course_id);
            }
            let s = idList.toString();
            sequelize.query("SELECT * FROM course WHERE id IN ("+s+")").spread((allCourses, metadata) => {
                let courseComplete = [];
                for (i = 0; i < allCourses.length; i++) {
                    courseComplete.push({id: allCourses[i].id, name: allCourses[i].name})
                } 
                res.status(200).json({courses: courseComplete});
            });
    })
});

router.post('/saveModuleSetup', (req, res, next) => {
    console.log(req.body);
    const ret = jwt.verify(req.body.token, secret);
    let fetchedStudent;
    Student.findOne({
        where: { email: ret.email }
    }).then(student => {
        if (!student) {
            res.status(401).json({
                message: "Token not valid"
            });
        }
        fetchedStudent = student;
        for (i = 0; i < req.body.collection.modules.length; i++) {
            const studentModule = new StudentModule({
                student_id: fetchedStudent.id,
                module_id: req.body.collection.modules[i]
            })
            studentModule.save()
        }
        Student.update(
            { setupComplete: true },
            { where: { email: fetchedStudent.email }}
        ).then(() => {
            res.status(201).json({
                complete: true
            });
        }).catch(err => {
            console.log(err);
        });
    })
})

// Get all modules for a given course
router.post('/getModules', (req, res, next) => {
    CourseModule.findAll({
        where: { course_id: req.body.courseId}
    }).then(modules => {
        let idList = [];
        for (i = 0; i < modules.length; i++) {
            idList.push(modules[i].module_id);
        }
        let s = idList.toString();
        sequelize.query("SELECT * FROM module WHERE id IN ("+s+")").spread((allModules, metadata) => {
            let moduleComplete = [];
            for (i = 0; i < allModules.length; i++) {
                moduleComplete.push({id: allModules[i].id, name: allModules[i].name, code: allModules[i].code});
            }
            res.status(200).json({modules: moduleComplete});
        })
    })
})

// Email confirmation route (link found in confirmation email)
router.get('/confirm/:token', (req, res, next) => {
    Student.update(
        { isEmailConfirmed: true},
        { where: { emailConfirmationToken: req.params.token }}
    ).then(()=> {
        res.redirect('http://localhost:4200/confirmed');
    })
});

// Get all universities
router.get('/getUnis', (req, res, next) => {
    University.findAll()
        .then(universities => {
            const unis = [];
            for (i = 0; i < universities.length; i++) {
                unis.push({id: universities[i].id, name: universities[i].name });
            }
            res.status(200).json({uni: unis});
        });
});



// Function for sending email confirmations
function sendEmailConfirmation(req, token) {
    const data = {
        from: 'Kurnel | Email Confirmation <confirmation@kurnel.com>',
        to: req.body.email,
        subject: '✔ Email Confirmation for kurnel.co',
        text: `Confirm your email address to complete your kurnel.co account registration associated with the email ${ req.body.email }. Follow the link below.\n\nhttp://localhost:3000/auth/confirm/${token}`
    }

    try {
       mailgun.messages().send(data)
    } catch (err) {
        console.log(err);
    }
}

module.exports = router;
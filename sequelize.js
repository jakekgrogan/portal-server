const Sequelize = require('sequelize');


const sequelize_dev_env = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: 'mysql',
        operatorsAliases: false,
        timezone: '+00:00',
        logging: false,
        define: {
            timestamps: false
        }
    }
);

const sequelize_prod_env = new Sequelize(
    process.env.RDS_DB_NAME,
    process.env.RDS_USERNAME,
    process.env.RDS_PASSWORD,
    {
        host: process.env.RDS_HOSTNAME,
        dialect: 'mysql',
        operatorsAliases: false,
        timezone: '+00:00',
        logging: false,
        define: {
            timestamps: false
        }
    }
);

module.exports = process.env.NODE_ENV === "production" ? sequelize_prod_env : sequelize_dev_env;
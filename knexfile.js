const dotenv = require('dotenv');

dotenv.load()

// if (process.env.NODE_ENV === "production") {
//     module.exports = {
//         client: 'mysql',
//         connection: process.env.DATABASE_URL || {
//             host: process.env.DB_HOST,
//             user: process.env.DB_USER,
//             password: process.env.DB_PASSWORD,
//             database: process.env.DB_NAME
//         }
//     }
// } else if (process.env.NODE_ENV === "development") {
//     module.exports = {
//         client: 'mysql',
//         connection: process.env.DATABASE_URL || {
//             host: process.env.DB_HOST,
//             user: process.env.DB_USER,
//             password: process.env.DB_PASSWORD,
//             database: process.env.DB_NAME
//         }
//     }
// }

module.exports = {
    client: 'mysql',
    connection: process.env.DATABASE_URL || {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    }
}

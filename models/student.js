const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Student = sequelize.define('student', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    fname:Sequelize.STRING(20),
    lname:Sequelize.STRING(20),
    college_id: Sequelize.STRING,
    email: { type: Sequelize.STRING(100), unique:true },
    password: Sequelize.STRING,
    course_code1: Sequelize.STRING(20),
    course_code2: Sequelize.STRING(20),
    createdAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    updatedAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
    passwordResetToken: Sequelize.STRING,
    passwordResetExpires: Sequelize.DATE,
    emailConfirmationToken: Sequelize.STRING,
    isEmailConfirmed: Sequelize.BOOLEAN,
    paymentMade: Sequelize.BOOLEAN,
    setupComplete: Sequelize.BOOLEAN,
}, {tableName: 'student'});

module.exports = Student;
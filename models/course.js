const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Course = sequelize.define('course', {
    id: { type: Sequelize.INTEGER(11), autoIncrement: true, primaryKey: true},
    name: Sequelize.STRING(45),
    code: Sequelize.STRING(45),
}, {tableName: 'course'});

module.exports = Course;
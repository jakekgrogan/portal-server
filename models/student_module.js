const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const StudentModule = sequelize.define('student_module', {
    student_id: { type: Sequelize.BIGINT(20)},
    module_id: Sequelize.BIGINT(20),
    id: { type: Sequelize.BIGINT(11), primaryKey: true, autoIncrement: true}
}, {tableName: 'student_module'});

module.exports = StudentModule;
const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const CourseModule = sequelize.define('course_module', {
    course_id: { type: Sequelize.INTEGER(11)},
    module_id: Sequelize.INTEGER(11),
    id: { type: Sequelize.INTEGER(11), primaryKey: true}
}, {tableName: 'course_module'});

module.exports = CourseModule;
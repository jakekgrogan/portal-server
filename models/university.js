const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const University = sequelize.define('university', {
    id: { type: Sequelize.BIGINT(20) , autoIncrement: true, primaryKey: true},
    name:Sequelize.STRING(45),
    street:Sequelize.STRING(45),
    city: Sequelize.STRING(45),
    country: Sequelize.STRING(45),
    email_end1: { type: Sequelize.STRING(45), unique: true },
    email_end2: Sequelize.STRING(45),
}, {tableName: 'university'});

module.exports = University;
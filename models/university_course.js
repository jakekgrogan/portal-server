const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const UniversityCourse = sequelize.define('university_course', {
    id: { type: Sequelize.INTEGER(11), autoIncrement: true, primaryKey: true},
    university_id: Sequelize.BIGINT(
        20),
    course_id: Sequelize.INTEGER(11),
}, {tableName: 'university_course'});

module.exports = UniversityCourse;
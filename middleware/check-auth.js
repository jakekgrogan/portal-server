const jwt = require('jsonwebtoken');
const secret = require('../auth/jwtsk');

// Middleware for user authorization
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, secret.privateKey);
        next();
    } catch (error) {
        res.status(401).json({
            message: "You dont have access to that resource"
        })
    }


};